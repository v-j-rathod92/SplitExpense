/*
 * MoneyBalance - Android-based calculator for tracking and balancing expenses
 * Copyright (C) 2012 Ingo van Lil <inguin@gmx.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lim.splitexpensefree.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.lim.splitexpensefree.MainActivity;
import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.SummaryFragment;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;
import com.lim.splitexpensefree.dao.ExpenseDataSource;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.data.Currency;
import com.lim.splitexpensefree.data.Expense;
import com.lim.splitexpensefree.data.Person;
import com.lim.splitexpensefree.export.CsvExporter;
import com.lim.splitexpensefree.helper.CurrencyHelper;
import com.lim.splitexpensefree.utils.Util;
import com.lim.splitexpensefree.utils.ManageCurrencyFragment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class ExpenseListFragment extends BaseFragment implements OnChildClickListener {

    public static final String PARAM_CALCULATION_ID = "calculationId";

    private long calculationId;

    private DataBaseHelper dbHelper;
    private CalculationDataSource calculationDataSource;
    private ExpenseDataSource expenseDataSource;

    private static final int ITEM_DELETE = 0;

    private ExpenseAdapter adapter;

    private class ExpenseAdapter extends BaseExpandableListAdapter {

        private boolean groupByPerson = true;

        private Calculation calculation;

        private final Map<Person, List<Expense>> expensesByPerson = new HashMap<>();

        private final Set<Calendar> dates = new TreeSet<>();
        private final Map<Calendar, List<Expense>> expensesByDate = new HashMap<>();

        private final LayoutInflater inflater;
        private final String groupSummaryFormat = getResources().getString(R.string.expenses_summary_total);
        private String fontLato;

        public ExpenseAdapter(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fontLato = "fonts/" + getString(R.string.font_lato);
        }

        public void setCalculation(Calculation calculation) {
            this.calculation = calculation;
            expenseDataSource = new ExpenseDataSource(dbHelper, calculation);

            expensesByPerson.clear();
            dates.clear();
            expensesByDate.clear();

            for (Person person : calculation.getPersons())
                expensesByPerson.put(person, new ArrayList<Expense>());

            for (Expense expense : calculation.getExpenses()) {
                List<Expense> byPersonList = expensesByPerson.get(expense.getPerson());
                byPersonList.add(expense);

                Calendar date = expense.getDate();
                List<Expense> byDateList = expensesByDate.get(date);
                if (byDateList == null) {
                    byDateList = new ArrayList<>();
                    expensesByDate.put(date, byDateList);
                    dates.add(date);
                }
                byDateList.add(expense);
            }

            notifyDataSetChanged();
        }

        public void setGroupByPerson(boolean groupByPerson) {
            this.groupByPerson = groupByPerson;
            notifyDataSetChanged();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public int getGroupCount() {
            if (calculation == null)
                return 0;
            else if (groupByPerson)
                return calculation.getPersons().size();
            else
                return dates != null ? dates.size() : 0;
        }

        @Override
        public long getGroupId(int groupPosition) {
            if (groupByPerson) {
                Person person = (Person) getGroup(groupPosition);
                return person.getId();
            } else {
                Calendar date = (Calendar) getGroup(groupPosition);
                return date.getTimeInMillis();
            }
        }

        @Override
        public Object getGroup(int groupPosition) {
            if (groupByPerson) {
                return calculation.getPersons().get(groupPosition);
            } else {
                return dates.toArray()[groupPosition];
            }
        }

        private class GroupViewHolder {
            public TextView nameView;
            public TextView txtTotalExpense;
            public TextView txtExpense;
            public ImageView addButton;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View view = convertView;
            GroupViewHolder holder;

            if (view == null) {
                view = inflater.inflate(R.layout.expense_list_group_row, parent, false);
                holder = new GroupViewHolder();
                holder.nameView = view.findViewById(R.id.txtPersonName);
                holder.txtTotalExpense = view.findViewById(R.id.txtTotalExpense);
                holder.txtExpense = view.findViewById(R.id.txtExpense);
                holder.addButton = view.findViewById(R.id.add_button);

                setTypeface(holder.nameView, fontLato);
                setTypeface(holder.txtTotalExpense, fontLato);
                setTypeface(holder.txtExpense, fontLato);

                view.setTag(holder);
            } else {
                holder = (GroupViewHolder) view.getTag();
            }

            List<Expense> expenses;

            if (groupByPerson) {
                Person person = (Person) getGroup(groupPosition);
                holder.nameView.setText(person.getName());
                expenses = expensesByPerson.get(person);
            } else {
                Calendar date = (Calendar) getGroup(groupPosition);
                DateFormat format = DateFormat.getDateInstance();
                holder.nameView.setText(format.format(date.getTime()));
                expenses = expensesByDate.get(date);
            }

            int count = 0;
            double total = 0;
            for (Expense expense : expenses) {
                count++;
                total += expense.getExchangedAmount();
            }

            if (count == 0) {
                holder.txtTotalExpense.setTextColor(ContextCompat.getColor(mContext, R.color.colorDarkGrey));
                holder.txtTotalExpense.setText(R.string.no_expenses);
                holder.txtExpense.setVisibility(View.GONE);
            } else {
                String totalStr = calculation.getMainCurrency().getCurrencyHelper().format(total);
                String totalAmount = String.format(groupSummaryFormat, totalStr);
                holder.txtTotalExpense.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));
                holder.txtTotalExpense.setText(count + " " + (count < 2 ? mContext.getString(R.string.expense) :
                        mContext.getString(R.string.expenses)));
                holder.txtExpense.setVisibility(View.VISIBLE);
                holder.txtExpense.setText(totalAmount);
            }

            holder.addButton.setOnClickListener(v -> {
                if (groupByPerson) {
                    Person person = (Person) getGroup(groupPosition);
                    addExpenseForPerson(person.getId());
                } else {
                    Calendar date = (Calendar) getGroup(groupPosition);
                    addExpenseForDate(date);
                }
            });

            return view;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            if (groupByPerson) {
                Person person = (Person) getGroup(groupPosition);
                List<Expense> list = expensesByPerson.get(person);
                return list.size();
            } else {
                Calendar date = (Calendar) getGroup(groupPosition);
                List<Expense> list = expensesByDate.get(date);
                return list.size();
            }
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            Expense expense = (Expense) getChild(groupPosition, childPosition);
            return expense.getId();
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            if (groupByPerson) {
                Person person = (Person) getGroup(groupPosition);
                List<Expense> list = expensesByPerson.get(person);
                return list.get(childPosition);
            } else {
                Calendar date = (Calendar) getGroup(groupPosition);
                List<Expense> list = expensesByDate.get(date);
                return list.get(childPosition);
            }
        }

        private class ChildViewHolder {
            public TextView titleView;
            public TextView amountView;
            public TextView exchangedView;
            public TextView details1View;
            public TextView details2View;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View view = convertView;
            ChildViewHolder holder;

            if (view == null) {
                view = inflater.inflate(R.layout.expense_row, parent, false);
                holder = new ChildViewHolder();
                holder.titleView = view.findViewById(R.id.expense_title);
                holder.amountView = view.findViewById(R.id.expense_amount);
                holder.exchangedView = view.findViewById(R.id.expense_exchanged_amount);
                holder.details1View = view.findViewById(R.id.expense_details_1);
                holder.details2View = view.findViewById(R.id.expense_details_2);

                setTypeface(holder.titleView, fontLato);
                setTypeface(holder.amountView, fontLato);
                setTypeface(holder.exchangedView, fontLato);
                setTypeface(holder.details1View, fontLato);
                setTypeface(holder.details2View, fontLato);

                view.setTag(holder);
            } else {
                holder = (ChildViewHolder) view.getTag();
            }

            Expense expense = (Expense) getChild(groupPosition, childPosition);
            Currency currency = expense.getCurrency();
            CurrencyHelper currencyHelper = currency.getCurrencyHelper();

            holder.titleView.setText(expense.getTitle());
            holder.amountView.setText(currencyHelper.format(expense.getAmount()));
            if (expense.getCurrency().equals(calculation.getMainCurrency())) {
                holder.exchangedView.setVisibility(View.GONE);
            } else {
                CurrencyHelper mainCurrencyHelper = calculation.getMainCurrency().getCurrencyHelper();
                double exchanged = expense.getExchangedAmount();
                holder.exchangedView.setVisibility(View.VISIBLE);
                holder.exchangedView.setText(mainCurrencyHelper.format(exchanged));
            }

            if (groupByPerson) {
                DateFormat format = DateFormat.getDateInstance();
                holder.details1View.setText(format.format(expense.getDate().getTime()));
            } else {
                Person person = expense.getPerson();
                holder.details1View.setText(person.getName());
            }

            if (expense.isUnevenSplit()) {
                List<Person> persons = calculation.getPersons();
                List<Double> shares = expense.getShares(persons);
                StringBuilder msg = new StringBuilder();
                for (int i = 0; i < persons.size(); i++) {
                    if (shares.get(i) > 0) {
                        Person person = persons.get(i);
                        if (msg.length() > 0)
                            msg.append("; ");
                        String shareStr = currencyHelper.format(shares.get(i));
                        msg.append(String.format("%s: %s", person.getName(), shareStr));
                    }
                }

                holder.details2View.setVisibility(View.VISIBLE);
                holder.details2View.setText(msg);
            } else {
                holder.details2View.setVisibility(View.GONE);
            }
            return view;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_expense_list, container, false);

        init();

        return rootView;
    }

    private void init() {
        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(null);

        ((AppCompatActivity) mContext).setSupportActionBar(toolbar);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowTitleEnabled(false);

        setHasOptionsMenu(true);
        displayBackButton(true);
        displayAddButton(false);

        Bundle args = getArguments();
        calculationId = args.getLong(PARAM_CALCULATION_ID);

        ExpandableListView listView = rootView.findViewById(R.id.expense_list);

        dbHelper = new DataBaseHelper(mContext);
        calculationDataSource = new CalculationDataSource(dbHelper);
        adapter = new ExpenseAdapter(mContext);
        listView.setAdapter(adapter);
        listView.setOnChildClickListener(this);
        registerForContextMenu(listView);

        refresh();
    }


    private void refresh() {
        Calculation calculation = calculationDataSource.get(calculationId);
        setTitle(calculation.getTitle());
        adapter.setCalculation(calculation);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.expense_list_options, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.group_by_person:
                adapter.setGroupByPerson(true);
                return true;
            case R.id.group_by_date:
                adapter.setGroupByPerson(false);
                return true;
            case R.id.manage_currencies:
                ManageCurrencyFragment manageCurrencyFragment = new ManageCurrencyFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(ManageCurrencyFragment.PARAM_CALCULATION_ID, calculationId);
                manageCurrencyFragment.setArguments(bundle);

                mTransition = mManager.beginTransaction();
                mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                mTransition.replace(R.id.container, manageCurrencyFragment);
                mTransition.addToBackStack(null);
                mTransition.commit();
                return true;
            case R.id.calculation_summary:
                MainActivity.addNewClick();

                SummaryFragment summaryFragment = new SummaryFragment();

                Bundle args = new Bundle();
                args.putLong(SummaryFragment.PARAM_CALCULATION_ID, calculationId);

                summaryFragment.setArguments(args);

                mTransition = mManager.beginTransaction();
                mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                mTransition.replace(R.id.container, summaryFragment);
                mTransition.addToBackStack(null);
                mTransition.commit();
                return true;
            case R.id.export_calculation:
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((AppCompatActivity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    exportCSVFile();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Expense expense = (Expense) adapter.getChild(groupPosition, childPosition);

        MainActivity.addNewClick();

        AddExpenseFragment addExpenseFragment = new AddExpenseFragment();
        Bundle args = new Bundle();

        args.putLong(AddExpenseFragment.PARAM_CALCULATION_ID, calculationId);
        args.putLong(AddExpenseFragment.PARAM_EXPENSE_ID, expense.getId());
        addExpenseFragment.setArguments(args);

        mTransition = mManager.beginTransaction();
        mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        mTransition.replace(R.id.container, addExpenseFragment);
        mTransition.addToBackStack(null);
        mTransition.commit();
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
//		if (v.getId() == R.id.expense_list) {
//			ExpandableListContextMenuInfo info = (ExpandableListContextMenuInfo) menuInfo;
//			int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
//			int child = ExpandableListView.getPackedPositionChild(info.packedPosition);
//			if (child != -1) {
//				Expense expense = (Expense) adapter.getChild(group, child);
//				menu.setHeaderTitle(expense.getTitle());
//				menu.add(0, ITEM_DELETE, 0, R.string.menu_delete);
//			}
//		}
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == ITEM_DELETE) {
            ExpandableListContextMenuInfo info = (ExpandableListContextMenuInfo) item.getMenuInfo();
            int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            int child = ExpandableListView.getPackedPositionChild(info.packedPosition);
            if (child != -1) {
                Expense expense = (Expense) adapter.getChild(group, child);
                expenseDataSource.delete(expense.getId());
                refresh();
            }
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        dbHelper.close();
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }

    private void addExpenseForPerson(long personId) {
        MainActivity.addNewClick();

        AddExpenseFragment addExpenseFragment = new AddExpenseFragment();
        Bundle args = new Bundle();

        args.putLong(AddExpenseFragment.PARAM_CALCULATION_ID, calculationId);
        args.putLong(AddExpenseFragment.PARAM_PERSON_ID, personId);
        addExpenseFragment.setArguments(args);

        mTransition = mManager.beginTransaction();
        mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        mTransition.replace(R.id.container, addExpenseFragment);
        mTransition.addToBackStack(null);
        mTransition.commit();
    }

    private void addExpenseForDate(Calendar date) {
        MainActivity.addNewClick();

        AddExpenseFragment addExpenseFragment = new AddExpenseFragment();
        Bundle args = new Bundle();

        args.putLong(AddExpenseFragment.PARAM_CALCULATION_ID, calculationId);
        args.putLong(AddExpenseFragment.PARAM_DATE, date.getTimeInMillis());
        addExpenseFragment.setArguments(args);

        mTransition = mManager.beginTransaction();
        mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        mTransition.replace(R.id.container, addExpenseFragment);
        mTransition.addToBackStack(null);
        mTransition.commit();
    }

    private void addExpense() {
        AddExpenseFragment addExpenseFragment = new AddExpenseFragment();
        Bundle args = new Bundle();

        args.putLong(AddExpenseFragment.PARAM_CALCULATION_ID, calculationId);
        addExpenseFragment.setArguments(args);

        mTransition = mManager.beginTransaction();
        mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        mTransition.replace(R.id.container, addExpenseFragment);
        mTransition.addToBackStack(null);
        mTransition.commit();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.imgBack:
                gotoBack();
                break;
        }
    }

    private void gotoBack() {
        mManager.popBackStack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                exportCSVFile();
            } else {
                Util.showSnackBar(rootView, getString(R.string.export_error_no_permission));
            }
        }
    }

    private void exportCSVFile() {
        CsvExporter.export(rootView, calculationDataSource.get(calculationId), mContext);
    }
}
