package com.lim.splitexpensefree.callback;

/**
 * Created by Vishal Rathod on 27/9/17.
 */

public interface OnSubjectDeleteListener {
    public void onItemDelete(long id);
}
