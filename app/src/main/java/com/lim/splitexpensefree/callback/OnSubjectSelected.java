package com.lim.splitexpensefree.callback;

import com.lim.splitexpensefree.data.Calculation;

/**
 * Created by Vishal Rathod on 18/9/17.
 */

public interface OnSubjectSelected {
    public void onSubjectSelected(Calculation calculation);
}
