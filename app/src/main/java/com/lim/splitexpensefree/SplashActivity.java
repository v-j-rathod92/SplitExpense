package com.lim.splitexpensefree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Lenovo on 01-10-2017.
 */

public class SplashActivity extends AppCompatActivity {
    private Thread mThread;
    private Context mContext;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

        mContext = this;

        mThread = new Thread(() -> {
            try {
                Thread.sleep(3000);
                handler.post(() -> {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        mThread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThread.interrupt();
    }
}
