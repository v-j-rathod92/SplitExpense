package com.lim.splitexpensefree.common;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Lenovo on 03-09-2017.
 */

@CoordinatorLayout.DefaultBehavior(MoveUpwardBehavior.class)
public class MovableLayout extends RelativeLayout {
    public MovableLayout(Context context) {
        super(context);
    }

    public MovableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MovableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
