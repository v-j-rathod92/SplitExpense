package com.lim.splitexpensefree.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.adapters.CurrencySpinnerAdapter;
import com.lim.splitexpensefree.adapters.PersonSpinnerAdapter;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;
import com.lim.splitexpensefree.dao.ExpenseDataSource;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.data.Currency;
import com.lim.splitexpensefree.data.Expense;
import com.lim.splitexpensefree.data.Person;
import com.lim.splitexpensefree.helper.CurrencyHelper;
import com.lim.splitexpensefree.utils.Util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vishal Rathod on 18/9/17.
 */

public class AddExpenseFragment extends BaseFragment implements AdapterView.OnItemSelectedListener {
    public static final String PARAM_EXPENSE_ID = "expenseId";
    public static final String PARAM_PERSON_ID = "personId";
    public static final String PARAM_CALCULATION_ID = "calculationId";
    public static final String PARAM_DATE = "date";

    private TextView txtTitleLabel, txtAmountLabel, txtPayerLabel, txtDateLabel;
    private EditText edtTxtTitle, edtTxtAmount, edtTxtSpinnerHelper, edtTxtSpinnerHelperPayer, edtTxtDate;
    private String fontLato, fontOpenSans;
    private Spinner spinnerCurrency, spinnerPayer;
    private ImageView imgDownArrow, imgEditDate;
    private Button btnAddSubject;
    private CheckBox chkSplit;

    private DataBaseHelper dbHelper;
    private CalculationDataSource calculationDataSource;
    private ExpenseDataSource expenseDataSource;
    private Calculation calculation;
    private CurrencyHelper currencyHelper;
    private Expense expense;
    private List<Person> persons;
    private CustomSplitEntry[] customSplitEntries;
    private TableLayout customSplitTable;

    private enum Mode {NEW_EXPENSE, EDIT_EXPENSE}

    private Mode mode;

    private static class CustomSplitEntry {
        CheckBox enabled;
        EditText weight;
        TextView result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_add_expense, container, false);
        init();
        setListeners();
        setTypeface();
        return rootView;
    }

    private void init() {
        setTitle(getString(R.string.add_subject2));
        displayBackButton(true);
        displayAddButton(false);
        displayDeleteButton(false);

        txtTitleLabel = rootView.findViewById(R.id.txtTitleLabel);
        txtAmountLabel = rootView.findViewById(R.id.txtAmountLabel);
        txtPayerLabel = rootView.findViewById(R.id.txtPayerLabel);
        txtDateLabel = rootView.findViewById(R.id.txtDateLabel);

        spinnerCurrency = rootView.findViewById(R.id.spinnerCurrency);
        spinnerPayer = rootView.findViewById(R.id.spinnerPayer);

        imgDownArrow = rootView.findViewById(R.id.imgDownArrow);
        imgEditDate = rootView.findViewById(R.id.imgEditDate);

        edtTxtTitle = rootView.findViewById(R.id.edtTxtTitle);
        edtTxtAmount = rootView.findViewById(R.id.edtTxtAmount);
        edtTxtSpinnerHelper = rootView.findViewById(R.id.edtTxtSpinnerHelper);
        edtTxtSpinnerHelperPayer = rootView.findViewById(R.id.edtTxtSpinnerHelperPayer);
        edtTxtDate = rootView.findViewById(R.id.edtTxtDate);

        customSplitTable = rootView.findViewById(R.id.tableLayoutSplitExpense);
        chkSplit = rootView.findViewById(R.id.chkSplit);
        btnAddSubject = (Button) rootView.findViewById(R.id.btnAddSubject);

        fontLato = "fonts/" + getString(R.string.font_lato);
        fontOpenSans = "fonts/" + getString(R.string.font_open_sans);

        long calculationId = getArguments().getLong(PARAM_CALCULATION_ID, -1);
        long expenseId = getArguments().getLong(PARAM_EXPENSE_ID, -1);

        dbHelper = new DataBaseHelper(mContext);
        calculationDataSource = new CalculationDataSource(dbHelper);

        calculation = calculationDataSource.get(calculationId);
        persons = calculation.getPersons();

        expenseDataSource = new ExpenseDataSource(dbHelper, calculation);

        mode = (expenseId >= 0 ? Mode.EDIT_EXPENSE : Mode.NEW_EXPENSE);
        if (mode == Mode.EDIT_EXPENSE) {
            setTitle(getString(R.string.edit_expense));
            expense = expenseDataSource.get(expenseId);
            edtTxtTitle.setText(expense.getTitle());
            btnAddSubject.setText(getString(R.string.edit_expense));
            displayDeleteButton(true);
        } else {
            setTitle(getString(R.string.new_expense));
            expense = new Expense(calculation);
            long personId = getArguments().getLong(PARAM_PERSON_ID, -1);
            expense.setPerson(calculation.getPersonById(personId));
            long millis = getArguments().getLong(PARAM_DATE, -1);

            if (millis > 0) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(millis);
                expense.setDate(cal);
            }
        }

        List<java.util.Currency> currencies = new ArrayList<>();
        for (Currency currency : calculation.getCurrencies())
            currencies.add(java.util.Currency.getInstance(currency.getCurrencyCode()));
        CurrencySpinnerAdapter adapter = new CurrencySpinnerAdapter(mContext, currencies);
        adapter.setSymbolOnly(true);

        Currency currency = expense.getCurrency();
        currencyHelper = currency.getCurrencyHelper();
        spinnerCurrency.setAdapter(adapter);
        spinnerCurrency.setSelection(adapter.findItem(currency.getCurrencyCode()));

        String[] personsArray = new String[persons.size()];
        int selected = -1;
        for (int i = 0; i < persons.size(); i++) {
            Person person = persons.get(i);
            personsArray[i] = person.getName();
            if (person.equals(expense.getPerson()))
                selected = i;
        }


        if (mode == Mode.EDIT_EXPENSE) {
            DecimalFormat df = new DecimalFormat("###.##");
//            String formatted = currencyHelper.format(expense.getAmount(), false);
            edtTxtAmount.setText(df.format(expense.getAmount()));
        }

        PersonSpinnerAdapter personSpinnerAdapter = new PersonSpinnerAdapter(mContext, persons);
        spinnerPayer.setAdapter(personSpinnerAdapter);
        if (selected != -1) {
            spinnerPayer.setSelection(selected);
        }

        updateDate();
        createCustomSplitRows();
        updateCustomSplit();
    }

    private void setListeners() {
        spinnerCurrency.setOnItemSelectedListener(this);
        spinnerPayer.setOnItemSelectedListener(this);

        if(spinnerCurrency.getCount() > 1){
            imgDownArrow.setOnClickListener(this);
            edtTxtSpinnerHelper.setOnClickListener(this);
        }
        edtTxtSpinnerHelperPayer.setOnClickListener(this);
        edtTxtDate.setOnClickListener(this);
        imgEditDate.setOnClickListener(this);
        btnAddSubject.setOnClickListener(this);
        edtTxtAmount.addTextChangedListener(updateCustomSplitTextWatcher);

        chkSplit.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            updateCustomSplit();
            customSplitTable.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });
    }

    private void setTypeface() {
        setTypeface(txtTitleLabel, fontOpenSans);
        setTypeface(txtAmountLabel, fontOpenSans);
        setTypeface(txtPayerLabel, fontOpenSans);
        setTypeface(txtDateLabel, fontOpenSans);
        setTypeface(chkSplit, fontOpenSans);

        setTypeface(edtTxtTitle, fontLato);
        setTypeface(edtTxtAmount, fontLato);
        setTypeface(edtTxtSpinnerHelperPayer, fontLato);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtTxtSpinnerHelper:
            case R.id.imgDownArrow:
                spinnerCurrency.performClick();
                break;

            case R.id.edtTxtSpinnerHelperPayer:
                spinnerPayer.performClick();
                break;

            case R.id.imgEditDate:
                pickDate();
                break;

            case R.id.btnAddSubject:
                if (validate()) {
                    try {
                        save();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.imgBack:
                gotoBack();
                break;

            case R.id.imgDelete:
                new AlertDialog.Builder(mContext)
                        .setTitle(getString(android.R.string.dialog_alert_title))
                        .setMessage(getString(R.string.confirm_delete_expense))
                        .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> delete())
                        .setNegativeButton(android.R.string.no, null).show();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        TextView txtSelected = ((TextView) view);
        String selected = txtSelected.getText().toString();

        if (adapterView.getId() == R.id.spinnerCurrency) {
            edtTxtSpinnerHelper.setText(selected);
            edtTxtSpinnerHelper.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));

            updateCurrency();
        } else if (adapterView.getId() == R.id.spinnerPayer) {
            expense.setPerson(persons.get(position));
            edtTxtSpinnerHelperPayer.setText(selected);
            edtTxtSpinnerHelperPayer.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void updateCurrency() {
        java.util.Currency c = (java.util.Currency) spinnerCurrency.getSelectedItem();
        for (Currency currency : calculation.getCurrencies())
            if (currency.getCurrencyCode().equals(c.getCurrencyCode()))
                expense.setCurrency(currency);
        currencyHelper = expense.getCurrency().getCurrencyHelper();
        updateCustomSplit();
    }

    private void pickDate() {
        final DatePickerDialog.OnDateSetListener onDateSet = (view, year, month, day) -> {
            Calendar date = Calendar.getInstance();
            date.clear();
            date.set(year, month, day);
            expense.setDate(date);
            updateDate();
        };

        Calendar date = expense.getDate();
        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONTH);
        int day = date.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, onDateSet, year, month, day);
        datePickerDialog.show();
    }

    private void updateDate() {
        DateFormat format = DateFormat.getDateInstance();
        edtTxtDate.setText(format.format(expense.getDate().getTime()));
    }


    private boolean validate() {
        if (edtTxtTitle.getText().toString().trim().length() == 0) {
            Util.showSnackBar(rootView, getString(R.string.validate_expense_title));
            return false;
        } else if (edtTxtAmount.getText().toString().trim().length() == 0) {
            Util.showSnackBar(rootView, getString(R.string.validate_expense_amount));
            return false;
        }

        return true;
    }

    private void createCustomSplitRows() {
        Map<Long, Double> weights = expense.getSplitWeights();
        customSplitEntries = new CustomSplitEntry[persons.size()];
        int dynamicId = 0;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        for (int i = 0; i < persons.size(); i++) {
            Person person = persons.get(i);
            boolean enabled = true;
            Double weight = 1.0;

            if (weights != null) {
                Double w = weights.get(person.getId());
                if (w != null)
                    weight = w;
                else
                    enabled = false;
            }

            TableRow row = (TableRow) inflater.inflate(R.layout.layout_split_row, customSplitTable, false);
            customSplitTable.addView(row);

            final CustomSplitEntry customSplitEntry = new CustomSplitEntry();
            customSplitEntries[i] = customSplitEntry;

            customSplitEntry.enabled = (CheckBox) row.findViewById(R.id.split_enabled);
            customSplitEntry.enabled.setId(dynamicId++);
            customSplitEntry.enabled.setText(person.getName() + ":");
            customSplitEntry.enabled.setChecked(enabled);
            customSplitEntry.enabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    customSplitEntry.weight.setEnabled(isChecked);
                    updateCustomSplit();
                }
            });

            customSplitEntry.weight = (EditText) row.findViewById(R.id.split_weight);
            customSplitEntry.weight.setId(dynamicId++);
            customSplitEntry.weight.setEnabled(enabled);
            customSplitEntry.weight.setText(currencyHelper.format(weight, false));
            customSplitEntry.weight.addTextChangedListener(updateCustomSplitTextWatcher);

            customSplitEntry.result = (TextView) row.findViewById(R.id.split_share);

            setTypeface(customSplitEntry.enabled, fontOpenSans);
            setTypeface(customSplitEntry.weight, fontLato);
            setTypeface(customSplitEntry.result, fontOpenSans);
        }

        chkSplit.setChecked(weights != null);
        customSplitTable.setVisibility(weights != null ? View.VISIBLE : View.GONE);
    }

    private void updateCustomSplit() {
        try {
            for (CustomSplitEntry customSplitEntry : customSplitEntries)
                customSplitEntry.result.setText("");

            double[] weights = new double[customSplitEntries.length];
            double weightSum = 0;
            for (int i = 0; i < customSplitEntries.length; i++) {
                weights[i] = 0;
                if (customSplitEntries[i].enabled.isChecked())
                    weights[i] = getWeight(i);
                weightSum += weights[i];
            }

            double amount = getAmount();
            for (int i = 0; i < customSplitEntries.length; i++) {
                if (customSplitEntries[i].enabled.isChecked() && weightSum > 0) {
                    double share = (weights[i] / weightSum * amount);
                    String formatted = currencyHelper.format(share);
                    customSplitEntries[i].result.setText(formatted);
                }
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private double getWeight(int i) throws ParseException {
        String amountString = customSplitEntries[i].weight.getText().toString();
        Number amountNumber = NumberFormat.getNumberInstance().parse(amountString);
        return amountNumber.doubleValue();
    }

    private final TextWatcher updateCustomSplitTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            updateCustomSplit();
        }
    };

    private void save() throws ParseException {
        expense.setTitle(getExpenseTitle());
        expense.setAmount(getAmount());

        Map<Long, Double> weights = null;
        if (chkSplit.isChecked()) {
            weights = new HashMap<>();
            for (int i = 0; i < customSplitEntries.length; i++)
                if (customSplitEntries[i].enabled.isChecked())
                    weights.put(persons.get(i).getId(), getWeight(i));
        }
        expense.setSplitWeights(weights);

        if (mode == Mode.EDIT_EXPENSE)
            expenseDataSource.update(expense);
        else
            expenseDataSource.insert(expense);

        mManager.popBackStack();
    }

    private void delete() {
        expenseDataSource.delete(expense.getId());
        gotoBack();
    }

    private String getExpenseTitle() {
        return edtTxtTitle.getText().toString().trim();
    }

    private double getAmount() throws ParseException {
        String amountString = edtTxtAmount.getText().toString();
        return currencyHelper.parse(amountString);
    }

    private void gotoBack() {
        mManager.popBackStack();
    }
}

