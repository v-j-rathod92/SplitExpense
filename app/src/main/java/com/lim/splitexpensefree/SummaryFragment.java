package com.lim.splitexpensefree;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.data.Expense;
import com.lim.splitexpensefree.data.Person;
import com.lim.splitexpensefree.fragments.BaseFragment;
import com.lim.splitexpensefree.helper.CurrencyHelper;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by Vishal Rathod on 26/9/17.
 */

public class SummaryFragment extends BaseFragment {
    public static final String PARAM_CALCULATION_ID = "calculationId";
    private DataBaseHelper dbHelper;
    private CalculationDataSource calculationDataSource;
    private CurrencyHelper currencyHelper;
    private String fontLato, fontOpenSans;
    private TextView txtSummaryLabel, txtExpenseConsumptionLabel, firstDateLabel, lastDateLabel, durationLabel,
            expensesLabel, totalAmountLabel, firstDateView, lastDateView, durationView, numExpensesView, totalAmountView,
            nameView, sumExpenses, sumConsumption, resultView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_summary, container, false);
        init();
        setTypeface();
        return rootView;
    }

    private void init() {

        txtSummaryLabel = rootView.findViewById(R.id.txtSummaryLabel);
        txtExpenseConsumptionLabel = rootView.findViewById(R.id.txtExpenseConsumptionLabel);
        firstDateLabel = rootView.findViewById(R.id.firstDateLabel);
        lastDateLabel = rootView.findViewById(R.id.lastDateLabel);
        durationLabel = rootView.findViewById(R.id.durationLabel);
        expensesLabel = rootView.findViewById(R.id.expensesLabel);
        totalAmountLabel = rootView.findViewById(R.id.totalAmountLabel);

        fontLato = "fonts/" + getString(R.string.font_lato);
        fontOpenSans = "fonts/" + getString(R.string.font_open_sans);

        long calculationId = getArguments().getLong(PARAM_CALCULATION_ID, -1);

        dbHelper = new DataBaseHelper(mContext);
        calculationDataSource = new CalculationDataSource(dbHelper);

        Calculation calculation = calculationDataSource.get(calculationId);
        currencyHelper = calculation.getMainCurrency().getCurrencyHelper();

        List<Person> persons = calculation.getPersons();
        List<Expense> expenses = calculation.getExpenses();

        if (expenses.isEmpty()) {
            TextView noExpensesView = (TextView) rootView.findViewById(R.id.no_expenses);
            noExpensesView.setVisibility(View.VISIBLE);
            TableLayout summaryTable = (TableLayout) rootView.findViewById(R.id.summary_table);
            summaryTable.setVisibility(View.GONE);
        } else {
            setSummary(calculation);
        }

        double[] totalExpenses = new double[persons.size()];
        double[] totalConsumption = new double[persons.size()];

        for (Expense expense : expenses) {
            List<Double> shares = expense.getExchangedShares(persons);
            for (int i = 0; i < persons.size(); i++) {
                if (persons.get(i).equals(expense.getPerson()))
                    totalExpenses[i] += expense.getExchangedAmount();
                totalConsumption[i] += shares.get(i);
            }
        }

        setTitle(calculation.getTitle());

        TableLayout table = (TableLayout) rootView.findViewById(R.id.results_table);
        LayoutInflater inflater = LayoutInflater.from(mContext);

        for (int i = 0; i < persons.size(); i++) {
            Person person = persons.get(i);

            TableRow row = (TableRow) inflater.inflate(R.layout.summary_row, table, false);
            table.addView(row);

            nameView = (TextView) row.findViewById(R.id.name);
            sumExpenses = (TextView) row.findViewById(R.id.sum_expenses);
            sumConsumption = (TextView) row.findViewById(R.id.sum_consumption);
            resultView = (TextView) row.findViewById(R.id.result);

            nameView.setText(person.getName() + ":");
            sumExpenses.setText(currencyHelper.format(totalExpenses[i]));
            sumConsumption.setText(currencyHelper.format(totalConsumption[i]));

            double result = totalExpenses[i] - totalConsumption[i];
            int color = getResources().getColor(result >= 0 ? R.color.result_positive : R.color.result_negative);
            resultView.setText(currencyHelper.format(result));
            resultView.setTextColor(color);
        }
    }

    private void setSummary(Calculation calculation) {
        firstDateView = (TextView) rootView.findViewById(R.id.first_date);
        lastDateView = (TextView) rootView.findViewById(R.id.last_date);
        durationView = (TextView) rootView.findViewById(R.id.duration);
        numExpensesView = (TextView) rootView.findViewById(R.id.num_expenses);
        totalAmountView = (TextView) rootView.findViewById(R.id.total_amount);

        DateFormat format = DateFormat.getDateInstance();
        firstDateView.setText(format.format(calculation.getFirstDate().getTime()));
        lastDateView.setText(format.format(calculation.getLastDate().getTime()));

        long duration = calculation.getDuration();
        String daysFormat = getResources().getString(duration == 1 ? R.string.day_format : R.string.days_format);
        durationView.setText(String.format(daysFormat, calculation.getDuration()));

        double totalAmount = 0;
        List<Expense> expenses = calculation.getExpenses();
        for (Expense expense : expenses)
            totalAmount += expense.getExchangedAmount();

        numExpensesView.setText(Integer.toString(expenses.size()));
        totalAmountView.setText(currencyHelper.format(totalAmount));
    }

    private void setTypeface() {
        setTypeface(txtSummaryLabel, fontLato);
        setTypeface(txtExpenseConsumptionLabel, fontLato);

        setTypeface(firstDateLabel, fontOpenSans);
        setTypeface(lastDateLabel, fontOpenSans);
        setTypeface(durationLabel, fontOpenSans);
        setTypeface(expensesLabel, fontOpenSans);
        setTypeface(totalAmountLabel, fontOpenSans);

        setTypeface(firstDateView, fontOpenSans);
        setTypeface(lastDateView, fontOpenSans);
        setTypeface(durationView, fontOpenSans);
        setTypeface(numExpensesView, fontOpenSans);
        setTypeface(totalAmountView, fontOpenSans);

        setTypeface(nameView, fontOpenSans);
        setTypeface(sumExpenses, fontOpenSans);
        setTypeface(sumConsumption, fontOpenSans);
        setTypeface(resultView, fontOpenSans);
    }
}
