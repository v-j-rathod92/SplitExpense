package com.lim.splitexpensefree.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lim.splitexpensefree.MainActivity;
import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.adapters.SubjectListAdapter;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;

/**
 * Created by Vishal Rathod on 14/9/17.
 */

public class SubjectListFragment extends BaseFragment {

    private TextView txtEmpty;
    private Button btnAddSubject;
    private LinearLayout layoutEmpty;
    private RecyclerView recyclerViewSubjectList;
    private String fontQuicksand, fontLato;
    private Cursor cursor;
    private int lastItemCount = -1;

    private DataBaseHelper dbHelper;
    private SubjectListAdapter adapter;
    private CalculationDataSource dataSource;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_manage_expense, container, false);
            init();
            setListeners();
            setTypeface();
        }
//        checkIfItemAdded();
        return rootView;
    }

    private void init() {
        setTitle(getString(R.string.expense_list));
        displayBackButton(false);

        txtEmpty = rootView.findViewById(R.id.txtEmpty);
        btnAddSubject = rootView.findViewById(R.id.btnAddSubject);
        layoutEmpty = rootView.findViewById(R.id.layoutEmpty);
        recyclerViewSubjectList = rootView.findViewById(R.id.recyclerViewSubjectList);

        fontLato = "fonts/" + getString(R.string.font_lato);
        fontQuicksand = "fonts/" + getString(R.string.font_quicksand);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerViewSubjectList.setLayoutManager(layoutManager);

        dbHelper = new DataBaseHelper(mContext);
        dataSource = new CalculationDataSource(dbHelper);
        cursor = dataSource.listAll();
    }

    private void setListeners() {
        btnAddSubject.setOnClickListener(this);
    }

    private void setTypeface() {
        setTypeface(txtEmpty, fontLato);
        setTypeface(btnAddSubject, fontQuicksand);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddSubject:
            case R.id.txtAdd:
                mTransition = mManager.beginTransaction();
                mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                mTransition.replace(R.id.container, new AddSubjectFragment());
                mTransition.addToBackStack(null);
                mTransition.commit();
                break;
        }
    }

    private void setAdapter(Cursor cursor) {
        if (cursor.getCount() == 0) {
            displayAddButton(false);
            layoutEmpty.setVisibility(View.VISIBLE);
            recyclerViewSubjectList.setVisibility(View.GONE);
        } else {
            displayAddButton(true);
            layoutEmpty.setVisibility(View.GONE);
            recyclerViewSubjectList.setVisibility(View.VISIBLE);
            adapter = new SubjectListAdapter(mContext, cursor, dataSource, recyclerViewSubjectList);
            adapter.setOnSubjectSelectedListener(calculation -> {
                mTransition = mManager.beginTransaction();

                MainActivity.addNewClick();

                ExpenseListFragment expenseListFragment = new ExpenseListFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(ExpenseListFragment.PARAM_CALCULATION_ID, calculation.getId());
                expenseListFragment.setArguments(bundle);

                mTransition.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                mTransition.replace(R.id.container, expenseListFragment);
                mTransition.addToBackStack(null);
                mTransition.commit();
            });

            adapter.setOnSubjectDeleteListener(id1 -> new AlertDialog.Builder(mContext)
                    .setTitle(getString(android.R.string.dialog_alert_title))
                    .setMessage(getString(R.string.confirm_delete_subject))
                    .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                        dataSource.delete(id1);
                        Cursor cursor1 = dataSource.listAll();
                        setAdapter(cursor1);
                    })
                    .setNegativeButton(android.R.string.no, null).show());
            recyclerViewSubjectList.setAdapter(adapter);

        }
        scrollToBottomIfItemAdded(cursor);
        lastItemCount = cursor.getCount();
    }

    @Override
    public void onResume() {
        super.onResume();
        Cursor cursor = dataSource.listAll();
        setAdapter(cursor);
    }

    /**
     * Check if item is added from @{@link AddSubjectFragment }, if yes then set adapter again
     * and move to last item of list.
     */
    private void scrollToBottomIfItemAdded(Cursor cursor) {
        if (cursor.getCount() != lastItemCount && lastItemCount != -1) {
            new Handler().postDelayed(() -> recyclerViewSubjectList.
                    getLayoutManager().scrollToPosition(cursor.getCount() - 1), 200);
        }
    }
}
