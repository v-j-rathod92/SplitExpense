package com.lim.splitexpensefree;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.lim.splitexpensefree.fragments.SubjectListFragment;

import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lim.splitexpensefree.storag.SharedPreferenceUtil;
import com.lim.splitexpensefree.utils.Util;

import java.util.List;

/**
 * Created by Vishal Rathod on 14/9/17.
 */

public class MainActivity extends AppCompatActivity {
    private AdView adView;
    private static InterstitialAd mInterstitialAd;
    private static Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        mContext = this;

        MobileAds.initialize(this, getString(R.string.admob_app_id));
        FirebaseAnalytics.getInstance(this);

        FragmentManager mManager = getSupportFragmentManager();
        FragmentTransaction mTransaction = mManager.beginTransaction();
        mTransaction.replace(R.id.container, new SubjectListFragment());
        mTransaction.commit();

        adView = (AdView) findViewById(R.id.adView);

        if (Util.isConnected(this)) {
            initializeAdvertise();
        }
    }

    public void initializeAdvertise() {
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                .build();

        AdListener adListener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        };

        adView.setAdListener(adListener);
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_id));
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                AdRequest adRequest = new AdRequest.Builder()
//                        .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                        .build();
                mInterstitialAd.loadAd(adRequest);
            }
        });
    }

    public static void addNewClick() {
        if (Util.isConnected(mContext)) {
            int totalClick = SharedPreferenceUtil.getInt("click", 0);
            Log.e("total click", totalClick + "");
            if (totalClick == 20) {
                if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
                SharedPreferenceUtil.putValue("click", 0);
            } else {
                totalClick = totalClick + 1;
                SharedPreferenceUtil.putValue("click", totalClick);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (adView != null) {
            adView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adView != null) {
            adView.destroy();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
