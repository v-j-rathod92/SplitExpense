package com.lim.splitexpensefree.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.data.Person;

import java.util.List;

/**
 * Created by Vishal Rathod on 18/9/17.
 */

public class PersonSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<Person> persons;
    private Typeface fontLato;

    public PersonSpinnerAdapter(Context mContext, List<Person> persons) {
        this.mContext = mContext;
        this.persons = persons;

        fontLato = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/" +
                mContext.getString(R.string.font_lato));
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        TextView view = (TextView) convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }

        view.setText(persons.get(i).getName());

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }

        view.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));
        view.setTypeface(fontLato);
        view.setText(persons.get(position).getName());
        return view;
    }
}
