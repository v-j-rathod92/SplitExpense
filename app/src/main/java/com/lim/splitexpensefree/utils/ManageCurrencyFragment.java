package com.lim.splitexpensefree.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.lim.splitexpensefree.fragments.BaseFragment;
import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.adapters.CurrencySpinnerAdapter;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.data.Currency;
import com.lim.splitexpensefree.data.Expense;
import com.lim.splitexpensefree.helper.CurrencyEditor;
import com.lim.splitexpensefree.helper.CurrencyHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 24-09-2017.
 */

public class ManageCurrencyFragment extends BaseFragment {
    private TextView txtMainCurrencyLabel, txtMainAdditionalCurrencyLabel;
    private RelativeLayout relativeLayoutCurrency;
    private EditText edtTxtSpinnerHelper;
    private Spinner spinnerCurrency;
    private ImageView imgDownArrow;
    private ListView listViewAdditionalCurrency;

    public static final String PARAM_CALCULATION_ID = "calculationId";
    private DataBaseHelper dbHelper;
    private CalculationDataSource calculationDataSource;
    private Calculation calculation;
    private final List<CurrencyEntry> additionalCurrencies = new ArrayList<>();
    private AdditionalCurrencyAdapter additionalCurrencyAdapter;
    private String fontLato, fontOpenSans;

    private class CurrencyEntry {
        public Currency currency;
        public boolean used;
    }

    private class AdditionalCurrencyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // plus one for "Add Currency"
            return additionalCurrencies.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.currency_list_row, parent, false);

            TextView textView = (TextView) view.findViewById(R.id.currency_details);
            setTypeface(textView, fontLato);
            ImageView deleteButton = (ImageView) view.findViewById(R.id.delete_button);
            textView.setId(position);

            boolean showDeleteButton = false;

            if (position < additionalCurrencies.size()) {
                final Currency thisCurrency = additionalCurrencies.get(position).currency;
                java.util.Currency newMainCurrency = (java.util.Currency) spinnerCurrency.getSelectedItem();
                CurrencyHelper mainCurrencyHelper = new CurrencyHelper(newMainCurrency);

                String text = String.format("%s (%s = %s)", thisCurrency.getCurrencyCode(),
                        thisCurrency.getCurrencyHelper().format(thisCurrency.getExchangeRateThis()),
                        mainCurrencyHelper.format(thisCurrency.getExchangeRateMain()));

                textView.setText(text);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showEditCurrencyDialog(thisCurrency);
                    }
                });

                showDeleteButton = !additionalCurrencies.get(position).used;

                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteCurrency(thisCurrency);
                    }
                });
            } else {
                textView.setText(R.string.add_currency);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showEditCurrencyDialog(null);
                    }
                });
            }

            deleteButton.setVisibility(showDeleteButton ? View.VISIBLE : View.INVISIBLE);
            return view;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_manage_currency, container, false);
        init();
        setListeners();
        setTypeface();
        return rootView;
    }

    private void init() {
        setTitle(getString(R.string.manage_currencies));
        displayBackButton(true);
        displayAddButton(true, getString(R.string.save));
        displayDeleteButton(false);

        dbHelper = new DataBaseHelper(mContext);
        calculationDataSource = new CalculationDataSource(dbHelper);

        long calculationId = getArguments().getLong(PARAM_CALCULATION_ID, -1);
        calculation = calculationDataSource.get(calculationId);

        txtMainCurrencyLabel = rootView.findViewById(R.id.txtMainCurrencyLabel);
        txtMainAdditionalCurrencyLabel = rootView.findViewById(R.id.txtMainAdditionalCurrencyLabel);
        relativeLayoutCurrency = rootView.findViewById(R.id.relativeLayoutCurrency);
        edtTxtSpinnerHelper = rootView.findViewById(R.id.edtTxtSpinnerHelper);
        spinnerCurrency = rootView.findViewById(R.id.spinnerCurrency);
        imgDownArrow = rootView.findViewById(R.id.imgDownArrow);
        listViewAdditionalCurrency = rootView.findViewById(R.id.listViewAdditionalCurrency);
        additionalCurrencyAdapter = new AdditionalCurrencyAdapter();

        fontLato = "fonts/" + getString(R.string.font_lato);
        fontOpenSans = "fonts/" + getString(R.string.font_open_sans);

        CurrencySpinnerAdapter mainCurrencyAdapter = new CurrencySpinnerAdapter(mContext);
        spinnerCurrency.setAdapter(mainCurrencyAdapter);
        int selected = mainCurrencyAdapter.findItem(calculation.getMainCurrencyCode());
        spinnerCurrency.setSelection(selected);

        update();
    }

    private void setListeners() {
        edtTxtSpinnerHelper.setOnClickListener(this);
        imgDownArrow.setOnClickListener(this);
        spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView txtSelected = ((TextView) view);
                String selected = txtSelected.getText().toString();
                edtTxtSpinnerHelper.setText(selected);
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setTypeface() {
        setTypeface(txtMainCurrencyLabel, fontOpenSans);
        setTypeface(txtMainAdditionalCurrencyLabel, fontOpenSans);
        setTypeface(edtTxtSpinnerHelper, fontLato);
    }

    private void update() {
        additionalCurrencies.clear();
        java.util.Currency newMainCurrency = (java.util.Currency) spinnerCurrency.getSelectedItem();

        for (Currency currency : calculation.getCurrencies()) {
            if (!currency.getCurrencyCode().equals(newMainCurrency.getCurrencyCode())) {
                CurrencyEntry entry = new CurrencyEntry();
                entry.currency = currency;
                entry.used = false;
                additionalCurrencies.add(entry);
            }
        }

        for (Expense expense : calculation.getExpenses())
            for (CurrencyEntry entry : additionalCurrencies)
                if (entry.currency.equals(expense.getCurrency()))
                    entry.used = true;

        listViewAdditionalCurrency.setAdapter(additionalCurrencyAdapter);
    }

    private void deleteCurrency(Currency currency) {
        if (!currency.equals(calculation.getMainCurrency()))
            calculation.getCurrencies().remove(currency);
        update();
    }

    private void showEditCurrencyDialog(final Currency currency) {
        List<Currency> hiddenCurrencies = new ArrayList<>();
        for (Currency c : calculation.getCurrencies())
            if (!c.equals(currency))
                hiddenCurrencies.add(c);
        CurrencyEditor editor = new CurrencyEditor(getActivity(), calculation.getMainCurrency(), hiddenCurrencies);
        if (currency != null)
            editor.setValue(currency);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(editor.getView());
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setNegativeButton(android.R.string.cancel, null);
        final AlertDialog dialog = builder.create();

        // register custom listener with validation for positive button
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface di) {
                Button okButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (editor.validate()) {
                            Currency newCurrency = editor.getValue(calculation.getId());
                            if (currency != null) {
                                currency.setExchangeRate(newCurrency.getExchangeRateThis(), newCurrency.getExchangeRateMain());
                            } else {
                                calculation.getCurrencies().add(newCurrency);
                            }
                            update();
                        }
                    }
                });
            }
        });
        dialog.show();
    }

    private void Save() {
        java.util.Currency newMainCurrency = (java.util.Currency) spinnerCurrency.getSelectedItem();
        calculation.setMainCurrencyCode(newMainCurrency.getCurrencyCode());
        calculationDataSource.update(calculation);
        gotoBack();

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.imgBack:
                gotoBack();
                break;

            case R.id.txtAdd:
                Save();
                break;

            case R.id.edtTxtSpinnerHelper:
            case R.id.imgDownArrow:
                spinnerCurrency.performClick();
                break;
        }
    }

    private void gotoBack() {
        mManager.popBackStack();
    }
}
