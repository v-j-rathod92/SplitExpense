package com.lim.splitexpensefree.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.callback.OnSubjectDeleteListener;
import com.lim.splitexpensefree.callback.OnSubjectSelected;
import com.lim.splitexpensefree.common.CursorRecyclerViewAdapter;
import com.lim.splitexpensefree.common.RecyclerViewAnimator;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.data.Expense;
import com.lim.splitexpensefree.data.Person;
import com.lim.splitexpensefree.helper.CurrencyHelper;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by Vishal Rathod on 13/9/17.
 */

public class SubjectListAdapter extends CursorRecyclerViewAdapter<SubjectListAdapter.ViewHolder> {

    private CalculationDataSource dataSource;
    private Typeface fontLato;
    private Context mContext;
    private RecyclerViewAnimator mAnimator;
    private OnSubjectSelected onSubjectSelected;
    private OnSubjectDeleteListener onSubjectDeleteListener;
    private String summaryFormat;

    public SubjectListAdapter(Context context, Cursor cursor, CalculationDataSource dataSource,
                              RecyclerView recyclerView) {
        super(context, cursor);
        this.mContext = context;
        this.dataSource = dataSource;

        mAnimator = new RecyclerViewAnimator(recyclerView);

        fontLato = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/" + context.getString(R.string.font_lato));

        summaryFormat = mContext.getString(R.string.expenses_summary_total);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Calculation calculation = dataSource.fromCursor(cursor);

        viewHolder.txtSubjectName.setText(calculation.getTitle().toUpperCase());

        StringBuilder personNames = new StringBuilder();
        for (Person person : calculation.getPersons()) {
            if (personNames.length() > 0) {
                personNames.append(", ");
            }
            personNames.append(person.getName());
        }

        viewHolder.txtPersonNames.setText(personNames);


        List<Expense> expenses = calculation.getExpenses();
        if (expenses.size() == 0) {
            viewHolder.txtExpenseDate.setVisibility(View.GONE);
            viewHolder.txtTotalExpense.setVisibility(View.GONE);
            viewHolder.txtExpense.setText(mContext.getString(R.string.no_expenses));
            viewHolder.txtExpense.setTextColor(ContextCompat.getColor(mContext, R.color.colorDarkGrey));
        } else {
            DateFormat format = DateFormat.getDateInstance();
            String firstDate = format.format(calculation.getFirstDate().getTime());
            String lastDate = format.format(calculation.getLastDate().getTime());
            viewHolder.txtExpenseDate.setText(String.format(mContext.getString(R.string.date_range_format), firstDate, lastDate));
            viewHolder.txtExpenseDate.setVisibility(View.VISIBLE);

            int count = calculation.getExpenses().size();
            CurrencyHelper helper = calculation.getMainCurrency().getCurrencyHelper();
            String total = helper.format(calculation.getExpenseTotal());
            viewHolder.txtTotalExpense.setText(count + " " + (count < 2 ? mContext.getString(R.string.expense) :
            mContext.getString(R.string.expenses)));
            viewHolder.txtExpense.setText(String.format(summaryFormat, total));
            viewHolder.txtExpense.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));
        }

        viewHolder.cardView.setOnClickListener(view -> onSubjectSelected.onSubjectSelected(calculation));

        viewHolder.imgDelete.setOnClickListener(view -> onSubjectDeleteListener.onItemDelete(calculation.getId()));
        mAnimator.onBindViewHolder(viewHolder.itemView, cursor.getPosition());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_subject_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        mAnimator.onCreateViewHolder(view);

        return viewHolder;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView txtSubjectName, txtPersonNames, txtExpenseDate, txtTotalExpense, txtExpense;
        private ImageView imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            txtSubjectName = itemView.findViewById(R.id.txtSubjectName);
            txtPersonNames = itemView.findViewById(R.id.txtPersonNames);
            txtExpenseDate = itemView.findViewById(R.id.txtExpenseDate);
            txtTotalExpense = itemView.findViewById(R.id.txtTotalExpense);
            txtExpense = itemView.findViewById(R.id.txtExpense);
            imgDelete = itemView.findViewById(R.id.imgDelete);

            txtSubjectName.setTypeface(fontLato);
            txtPersonNames.setTypeface(fontLato);
            txtExpenseDate.setTypeface(fontLato);
            txtTotalExpense.setTypeface(fontLato);
            txtExpense.setTypeface(fontLato);
        }
    }

    public void setOnSubjectSelectedListener(OnSubjectSelected onSubjectSelected){
        this.onSubjectSelected = onSubjectSelected;
    }

    public void setOnSubjectDeleteListener(OnSubjectDeleteListener onSubjectDeleteListener){
        this.onSubjectDeleteListener = onSubjectDeleteListener;
    }
}
