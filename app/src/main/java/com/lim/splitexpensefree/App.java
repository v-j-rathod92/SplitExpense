package com.lim.splitexpensefree;

import android.app.Application;

import com.lim.splitexpensefree.storag.SharedPreferenceUtil;

/**
 * Created by Vishal Rathod on 13/9/17.
 */

public class App extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferenceUtil.init(this);

    }
}
