package com.lim.splitexpensefree.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by Vishal Rathod on 12/9/17.
 */

public class Util {
    public static Currency getDefaultCurrency() {
        Locale locale = Locale.getDefault();
        return Currency.getInstance(locale);
    }

    public static void showSnackBar(View rootView, String message){
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    private static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }
}
