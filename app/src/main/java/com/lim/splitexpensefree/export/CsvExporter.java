package com.lim.splitexpensefree.export;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.utils.Util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class CsvExporter {

	static private final int MAX_FILENUM = 1000;

	public static void export(View rootView, Calculation calculation, Context context) {
		Resources res = context.getResources();

		File root = Environment.getExternalStorageDirectory();
		if (!root.canWrite()) {
			Util.showSnackBar(rootView, res.getString(R.string.export_error_no_permission));
			return;
		}

		File dir = new File(root, res.getString(R.string.app_name));
		if (!dir.exists() && !dir.mkdir()) {
			String message = String.format(res.getString(R.string.export_error_mkdir), dir.toString());
			Util.showSnackBar(rootView, message);
			return;
		}

		File csvFile = determineFileName(calculation, dir);
		if (csvFile == null) {
			Util.showSnackBar(rootView, res.getString(R.string.export_error_no_free_file));
			return;
		}

		try {
			FileWriter writer = new FileWriter(csvFile);
			CsvOutput csv = new CsvOutput(calculation);
			writer.write(csv.toCsv());
			writer.close();

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO)
				triggerMediaRescan(csvFile, context);

			String message = String.format(res.getString(R.string.export_success), csvFile.toString());
			Util.showSnackBar(rootView, message);
		} catch (IOException e) {
			String message = String.format(res.getString(R.string.export_error_write_failed), csvFile.toString());
			Util.showSnackBar(rootView, message);
			Log.e("moneybalance", message, e);
		}
	}

	private static File determineFileName(Calculation calculation, File dir) {
		String base = calculation.getTitle();
		final char[] ILLEGAL_CHARS = { '\\', '/', '<', '>', '?', ':', '*', '|', '"', '\'' };
		for (char illegal : ILLEGAL_CHARS)
			base = base.replace(illegal, '_');

		File file = new File(dir, base + ".csv");
		if (!file.exists())
			return file;

		for (int i = 1; i < MAX_FILENUM; i++) {
			file = new File(dir, String.format("%s (%d).csv", base, i));
			if (!file.exists())
				return file;
		}
		return null;
	}

	@TargetApi(Build.VERSION_CODES.FROYO)
	private static void triggerMediaRescan(File file, Context context) {
		MediaScannerConnection.scanFile(context, new String[] { file.getAbsolutePath() }, null, null);
	}

}
