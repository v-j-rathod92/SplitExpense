package com.lim.splitexpensefree.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.lim.splitexpensefree.R;
import com.lim.splitexpensefree.adapters.CurrencySpinnerAdapter;
import com.lim.splitexpensefree.dao.CalculationDataSource;
import com.lim.splitexpensefree.dao.DataBaseHelper;
import com.lim.splitexpensefree.data.Calculation;
import com.lim.splitexpensefree.utils.Util;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Vishal Rathod on 14/9/17.
 */

public class AddSubjectFragment extends BaseFragment implements AdapterView.OnItemSelectedListener{
    private TextView txtSubjectLabel, txtCurrencyLabel, txtCurrencyPersons;
    private EditText edtTxtSubject, edtTxtSpinnerHelper;
    private Spinner spinnerCurrency;
    private ImageView imgDownArrow;
    private Button btnAddSubject;
    private final List<AddSubjectFragment.PersonView> listPersonViews = new ArrayList<>();
    private LinearLayout linearLayoutPersonList;
    private String fontLato, fontOpenSans;

    private static final int MIN_PERSONS = 2;
    private static final int MAX_PERSONS = 100;

    private class PersonView {
        public View view;
        public EditText edtTxtPersonName;
        public ImageView imgDelete;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_add_subject, container, false);
        init();
        setListeners();
        setTypeface();
        return rootView;
    }

    private void init() {
        setTitle(getString(R.string.add_subject2));
        displayBackButton(true);
        displayAddButton(false);

        txtSubjectLabel = rootView.findViewById(R.id.txtSubjectLabel);
        txtCurrencyLabel = rootView.findViewById(R.id.txtCurrencyLabel);
        txtCurrencyPersons = rootView.findViewById(R.id.txtCurrencyPersons);
        edtTxtSubject = rootView.findViewById(R.id.edtTxtSubject);
        edtTxtSpinnerHelper = rootView.findViewById(R.id.edtTxtSpinnerHelper);
        spinnerCurrency = rootView.findViewById(R.id.spinnerCurrency);
        btnAddSubject = rootView.findViewById(R.id.btnAddSubject);
        linearLayoutPersonList = rootView.findViewById(R.id.linearLayoutPersonList);
        imgDownArrow = rootView.findViewById(R.id.imgDownArrow);

        fontLato = "fonts/" + getString(R.string.font_lato);
        fontOpenSans = "fonts/" + getString(R.string.font_open_sans);

        CurrencySpinnerAdapter currencySpinnerAdapter = new CurrencySpinnerAdapter(mContext);
        spinnerCurrency.setAdapter(currencySpinnerAdapter);
        int selected = currencySpinnerAdapter.findItem(Util.getDefaultCurrency());
        spinnerCurrency.setSelection(selected);

        createOrDeletePersonRows();
    }

    private void setListeners() {
        spinnerCurrency.setOnItemSelectedListener(this);
        edtTxtSpinnerHelper.setOnClickListener(this);
        btnAddSubject.setOnClickListener(this);
        imgDownArrow.setOnClickListener(this);
    }

    private void setTypeface() {
        setTypeface(txtSubjectLabel, fontOpenSans);
        setTypeface(txtCurrencyLabel, fontOpenSans);
        setTypeface(txtCurrencyPersons, fontOpenSans);
        setTypeface(edtTxtSubject, fontLato);
        setTypeface(btnAddSubject, fontLato);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                gotoBack();
                break;

            case R.id.edtTxtSpinnerHelper:
            case R.id.imgDownArrow:
                spinnerCurrency.performClick();
                break;

            case R.id.btnAddSubject:
                if (validate()) {
                    String title = edtTxtSubject.getText().toString().trim();
                    Currency currency = (Currency) spinnerCurrency.getSelectedItem();
                    List<String> listPersonNames = getPersonNames();

                    DataBaseHelper dataBaseHelper = new DataBaseHelper(mContext);
                    CalculationDataSource dataSource = new CalculationDataSource(dataBaseHelper);
                    Calculation calculation = dataSource.createCalculation(title, currency.getCurrencyCode(), listPersonNames);
                    dataBaseHelper.close();

                    gotoBack();
                }
                break;
        }
    }

    private boolean validate() {
        if (edtTxtSubject.getText().toString().trim().length() == 0) {
            Util.showSnackBar(rootView.findViewById(R.id.scrollBar), getString(R.string.validate_subject_name));
            return false;
        }

        Set<String> personNames = new HashSet<>();
        for (int i = 0; i < listPersonViews.size(); i++) {
            String name = getPersonName(i);

            if (name.length() > 0) {
                if (personNames.contains(name)) {
                    Util.showSnackBar(rootView.findViewById(R.id.scrollBar), getString(R.string.validate_duplicate_name));
                    return false;
                } else {
                    personNames.add(name);
                }
            }
        }

        if (personNames.size() < MIN_PERSONS) {
            Util.showSnackBar(rootView.findViewById(R.id.scrollBar), getString(R.string.validate_minimum_person));
            return false;
        }

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        TextView txtSelected = ((TextView) view);
        String selected = txtSelected.getText().toString();
        edtTxtSpinnerHelper.setText(selected);
        edtTxtSpinnerHelper.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlack));
        setTypeface(edtTxtSpinnerHelper, fontLato);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void createOrDeletePersonRows() {
        int numEmpty = 0;
        int i = 0;
        while (i < listPersonViews.size() && listPersonViews.size() >= 2) {
            AddSubjectFragment.PersonView personView = listPersonViews.get(i++);
            String name = personView.edtTxtPersonName.getText().toString().trim();
            if (name.length() == 0) {
                if (!personView.edtTxtPersonName.hasFocus()) {
                    deletePersonRow(personView);
                    i--;
                } else {
                    numEmpty++;
                }
            }
        }

        while ((listPersonViews.size() < MIN_PERSONS) ||
                (numEmpty < 1 && listPersonViews.size() < MAX_PERSONS)) {
            addPersonRow();
            numEmpty++;
        }

        for (i = 0; i < listPersonViews.size(); i++) {
            AddSubjectFragment.PersonView personView = listPersonViews.get(i);
            boolean last = (i + 1 == listPersonViews.size());
            personView.imgDelete.setVisibility(last ? View.INVISIBLE : View.VISIBLE);
        }
    }

    private void deletePersonRow(AddSubjectFragment.PersonView row) {
        linearLayoutPersonList.removeView(row.view);
        listPersonViews.remove(row);
    }

    private AddSubjectFragment.PersonView addPersonRow() {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_row_person, linearLayoutPersonList, false);
        linearLayoutPersonList.addView(view);

        final AddSubjectFragment.PersonView personView = new AddSubjectFragment.PersonView();
        personView.view = view;
        personView.edtTxtPersonName = view.findViewById(R.id.edtTxtPersonName);
        personView.imgDelete = view.findViewById(R.id.imgDelete);
        listPersonViews.add(personView);

        setTypeface(personView.edtTxtPersonName, fontLato);

        personView.edtTxtPersonName.setId(-1); // do not restore in onRestoreInstanceState()
        personView.edtTxtPersonName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                createOrDeletePersonRows();
            }
        });

        personView.edtTxtPersonName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                createOrDeletePersonRows();
        });

        personView.imgDelete.setOnClickListener(v -> {
            deletePersonRow(personView);
            createOrDeletePersonRows();
        });
        return personView;
    }

    private String getPersonName(int position) {
        AddSubjectFragment.PersonView personView = listPersonViews.get(position);
        return personView.edtTxtPersonName.getText().toString().trim();
    }

    private ArrayList<String> getPersonNames() {
        ArrayList<String> listPersonNames = new ArrayList<>();
        for (int i = 0; i < listPersonViews.size(); i++) {
            String name = getPersonName(i);
            if (name.length() > 0)
                listPersonNames.add(name);
        }

        return listPersonNames;
    }

    private void gotoBack() {
        mManager.popBackStack();
    }
}
